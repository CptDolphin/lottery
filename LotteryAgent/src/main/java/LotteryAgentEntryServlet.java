import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class LotteryAgentEntryServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LotteryAgentEntryServlet.class);

    private List<Integer> createRandomNumbers() {
        return new Random()
                .ints(6,1,49)
                .mapToObj(Integer::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("in doGet message" + req.getRequestedSessionId());

        CloseableHttpClient httpClient = HttpClients.createDefault();
        List<Integer> randomNumbers = createRandomNumbers();

        HttpPost httpPost = new HttpPost("http://localhost:8080/lotteryBoss/api/numbers");
        httpPost.setEntity(new StringEntity(new Gson().toJson(randomNumbers)));
        httpPost.setHeader(new BasicHeader("content-type","application/json"));
        HttpResponse lotteryBossResponse = httpClient.execute(httpPost);

        String lotteryBossResponseString =
                new BasicResponseHandler()
                .handleResponse(lotteryBossResponse);
        resp.getWriter().write("Your numbers: " + randomNumbers.toString());
        resp.getWriter().write(lotteryBossResponseString);
    }
}
