import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Startup
@Singleton
public class NumberGenerator {

    private static final Logger logger = Logger.getLogger(NumberGenerator.class);

    private List<Integer> winningNumbers;

    private List<Integer> drawWinningNumbers() {
        return new Random()
                .ints(6,1,49)
                .mapToObj(Integer::valueOf)
                .collect(Collectors.toList());
    }

    @PostConstruct
    public void generateNumbersOnStart() {
        winningNumbers = drawWinningNumbers();
        logger.info(winningNumbers.toArray().toString());
    }

    public List<Integer> getWinningNumbers(){
        return winningNumbers;
    }

}
