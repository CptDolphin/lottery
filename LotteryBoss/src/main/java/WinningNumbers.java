import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/numbers")
public class WinningNumbers {

    @Inject
    private NumberGenerator numberGenerator;

    @GET
    @Path("/winning")
    public Response getWinningNumbers() {
        numberGenerator.generateNumbersOnStart();
        String winningNumbers = String.valueOf(numberGenerator.getWinningNumbers());

        return Response.ok(winningNumbers).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getLotteryResults(List<Integer> list) {
        Boolean go = isSuccess(list, numberGenerator.getWinningNumbers());
        if (go == true) {
            return Response.ok(numberGenerator.getWinningNumbers() + "\n" + "Congratulation").build();
        }
        if (go == false) {
            return Response.ok(numberGenerator.getWinningNumbers()+"\n" + "Try again").build();
        } else {
            return Response.ok("Something went wrong").build();
        }
    }

    private boolean isSuccess(List<Integer> numbers, List<Integer> winningNumbers) {
        return winningNumbers
                .stream()
                .anyMatch(number -> numbers.contains(number));
    }
}
