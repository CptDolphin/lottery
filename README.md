# Lottery task by Rafal Walas

This project contains of two separate projects - one being LotteryAgent(you) generating numbers for the lottery and second being LotteryBoss generating one set of static numbers written to logs at the start of program and put on web as Json.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
1. Java IDE avaible of running JEE projects
2. Linking Tomcat with lotteryAgent
3. Linking wildfly with lotteryBoss 
```

### Installing

A step by step series of examples that tell you have to get a development env running

```
In this project there are 2 programs needed to be run

1. Firsly open you IDE and run the first program by clicking LotteryBoss and then pom.xml
2. Secondly open second file - LotteryAgent and also click pom.xml
3. Download tomcat from  : https://tomcat.apache.org/download-80.cgi
4. Depending on IDE; In lotteryAgent go to Run/Debug Configuration (in Intelij you can find it in top right corner)
5. Add new Configuration 
6. Select Tomcat and local
7. Select folder to which you installed Tomcat server and subcategory local, click fix to add war file in bottom right corner and then accept
note: we are going to run two diffrent servers so one of the ports need to be switched for example tomcat at 8085 and widlfly at 8080.
8. Download wildfly from  : http://wildfly.org/downloads/ (162MB version would be best)
9. Go to LotteryBoss and create .war file - in Intelij you can do it by pressing 
    View -> Tool Windows -> Mayven Project -> Lifecycle and with ctrl press Clean, Install and play
10. Now copy the .war file created in Target directory in project
11. Go to place where you downloaded wildfly and go to 
        \wildfly\standalone\deployments and place .war here
12. Now start wildfly by going to:
        \wildfly\bin\standalone.bat by double clicking or running it from bash 
13. Go back to lotteryAgent project and press green triangle top right to start tomcat
14. Finish it up by adding /play at the end of directory like this : http://localhost:8085/play

```

## Built With

* [Maven](https://maven.apache.org/)
* [Wildfly](http://wildfly.org/) 
* [Tomcat](http://tomcat.apache.org/) 